#zamiana temperatury z C na F

def celsiusToFahrenheit(cTemp):
    return map(lambda t: (float(9)/5)*t + 32, cTemp)


cTemp = [36.6, 37.1, 38.0, 38.8]
fTemp = celsiusToFahrenheit(cTemp)
print fTemp